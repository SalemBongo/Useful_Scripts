# -----------------------------------------------------------------------------
# Project:       Coin Flip
# Description:
# Version:       v1.3.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# -----------------------------------------------------------------------------

from random import random


def flip(number):
    heads = 0
    backs = 0
    result = ''
    for i in range(int(number)):
        if random() >= 0.5:
            heads += 1
            result += '[Head]'
        else:
            backs += 1
            result += '[Back]'
    print(f'Heads: {heads}')
    print(f'Backs: {backs}')
    print(result)


if __name__ == '__main__':
    flip(input('How many flips? '))
