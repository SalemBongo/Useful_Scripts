# ---------------------------------------------------------------------------------------------------------------------
# Project:       Chat
# Description:
# Version:       v1.1.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

import socket
import time


host = socket.gethostbyname(socket.gethostname())
port = 9009
serv_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
serv_sock.bind((host, port))


quit = False
clients = []


while not quit:
    try:
        data, addr = serv_sock.recvfrom(1024)
        if addr not in clients:
            clients.append(addr)
        now = time.strftime('%Y-%m-%d-%H.%M.%S', time.localtime())
        for client in clients:
            if addr != client:
                serv_sock.sendto(data, client)
    except Exception:
        quit = True


serv_sock.close()
