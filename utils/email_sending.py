# ---------------------------------------------------------------------------------------------------------------------
# Project:       Sending E-mail
# Description:
# Version:       v1.3.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

from smtplib import SMTP
from os.path import basename
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


email_from = ''
email_password = ''
send_to_email = ''
subject = ''
message = ''
file = r'\home\file.txt'  # path to file


msg = MIMEMultipart()
msg['From'] = email_from
msg['To'] = send_to_email
msg['Subject'] = subject


msg.attach(MIMEText(message, 'plain'))
filename = basename(file)
attachment = open(file, 'rb')
part = MIMEBase('application', 'octet-stream')
part.set_payload(attachment.read())
encoders.encode_base64(part)
part.add_header('Content-Disposition', f'attachment; filename={filename}')
msg.attach(part)


if __name__ == '__main__':
    server = SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(email_from, email_password)
    text = msg.as_string()
    server.sendmail(email_from, send_to_email, text)
    server.quit()
