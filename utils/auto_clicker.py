# ---------------------------------------------------------------------------------------------------------------------
# Project:       Auto Clicker
# Description:
# Version:       v1.1.1  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

from time import sleep
from threading import Thread
from pynput.mouse import Button, Controller
from pynput.keyboard import Listener, KeyCode


delay = 0.001
button = Button.left
start_stop_key = KeyCode(char='s')
exit_key = KeyCode(char='e')


class ClickMouse(Thread):
    def __init__(self, delay, button):
        super(ClickMouse, self).__init__()
        self.delay = delay
        self.button = button
        self.running = False
        self.program_running = True

    def start_clicking(self):
        self.running = True

    def stop_clicking(self):
        self.running = False

    def exit(self):
        self.stop_clicking()
        self.program_running = False

    def run(self):
        while self.program_running:
            while self.running:
                mouse.click(self.button)
                sleep(self.delay)
            sleep(0.1)


mouse = Controller()
click_thread = ClickMouse(delay, button)
click_thread.start()


def on_press(key):
    if key == start_stop_key:
        if click_thread.running:
            click_thread.stop_clicking()
        else:
            click_thread.start_clicking()
    elif key == exit_key:
        click_thread.exit()
        listener.stop()


if __name__ == '__main__':
    with Listener(on_press=on_press) as listener:
        listener.join()
