# ---------------------------------------------------------------------------------------------------------------------
# Project:       Brute Force (.zip)
# Description:
# Version:       v1.1.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

from datetime import datetime
from os import mkdir
from zipfile import ZipFile


def create_folder():
    folder = 'Extracted'
    try:
        mkdir(folder)
    except Exception as e:
        print(e)
        pass


start_time = datetime.now()


def generator(string):
    for w in string:
        psswrd = w.replace('\n', '')
        archive.setpassword(psswrd.encode())
        try:
            archive.extractall(create_folder())
        except Exception:
            yield f'Fail: {psswrd}'
        else:
            finish_time = datetime.now()
            yield f'Success: {psswrd}'
            delta = finish_time - start_time
            yield f'Spent time: {delta}'
            return string


with open(input('Bank with keys: ')) as data:
    with ZipFile(input('Archive to unlock: ')) as archive:
        count = 0
        for psswrd in generator(data):
            count += 1
            print(f'Try # {count}')
            print(psswrd)
