# ---------------------------------------------------------------------------------------------------------------------
# Project:       Cleaner
# Description:
# Version:       v1.1.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

import os
import time


folders = ['\home\Trash',
           '\home\Logs']
days = 2
total_deleted_size = 0
total_deleted_file = 0
total_deleted_dirs = 0

now_time = time.time()
age_time = now_time - 60 * 60 * 24 * days


def delete_old_files(folder):
    global total_deleted_file
    global total_deleted_size
    for path, dirs, files in os.walk(folder):
        for file in files:
            filename = os.path.join(path, file)  # full path to file
            filetime = os.path.getmtime(filename)
            if filetime < age_time:
                size_file = os.path.getsize(filename)
                total_deleted_size += size_file
                total_deleted_file += 1
                print(f'Deleting file: {filename}')
                os.remove(filename)


def delete_empty_dirs(folder):
    global total_deleted_dirs
    empty_folders_now = 0
    for path, dirs, files in os.walk(folder):
        if not dirs and not files:
            total_deleted_dirs += 1
            empty_folders_now += 1
            print(f'Deleting empty dir: {path}')
            os.rmdir(path)
    if empty_folders_now > 0:
        delete_empty_dirs(folder)


start_t = time.asctime()
for folder in folders:
    delete_old_files(folder)
    delete_empty_dirs(folder)
finish_t = time.asctime()

print(f'Start time: {start_t}')
print(f'Total deleted size {total_deleted_size/1024/1024} Mb')
print(f'Total deleted files {total_deleted_file}')
print(f'Total deleted folders {total_deleted_dirs}')
print(f'Finish time: {finish_t}')
