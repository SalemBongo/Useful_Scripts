# ---------------------------------------------------------------------------------------------------------------------
# Project:       Purge Log
# Description:
# Version:       v1.1.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

from shutil import copyfile
from os import path, stat
from sys import argv


def purge_log(filename, limit_size, log_number):
    """
    Ex: python3 purge_log.py _log.txt 10 5
    """
    if len(argv) < 4:
        print('Missing argument')
    if path.isfile(filename):
        logfile_size = stat(filename).st_size
        logfile_size = logfile_size / 1024
        if logfile_size >= limit_size:
            if log_number > 0:
                for current_file_number in range(log_number, 1, -1):
                    src = f'{filename}_{current_file_number - 1}'
                    dst = f'{filename}_{current_file_number}'
                    if path.isfile(src):
                        copyfile(src, dst)
                        print(f'Copied {src} to {dst}')
                copyfile(filename, filename + '_1')
                print(f'Copied {filename} to {filename + "_1"}')
                with open(filename, 'w'):
                    pass


if __name__ == '__main__':
    purge_log(argv[1], int(argv[2]), int(argv[3]))
