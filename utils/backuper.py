# ---------------------------------------------------------------------------------------------------------------------
# Project:       Backuper
# Description:
# Version:       v1.2.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

import os
import time


source = ['\home\example2', '\home\example2']
target_dir = r'\home\backups'


today = target_dir + os.sep + time.strftime('%Y%m%d')
now = time.strftime('%H%M%S')


comment = input('Enter comment: ')
if len(comment) == 0:
    target = today + os.sep + now + '.zip'
else:
    target = today + os.sep + now + '_' + comment.replace(' ', '_') + '.zip'


if not os.path.exists(today):
    os.mkdir(today)
print(f'[Done] Catalog: {today}')


command = f'zip -qr {target} {" ".join(source)}'


try:
    if os.system(command) == 0:
        print(f'[Done] Backup in {target}')
except Exception as e:
    print(f'[Error] {e}')
